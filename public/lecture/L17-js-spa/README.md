---
author: mos
revision: 
    2022-12-13: "(A, mos) first version"
---
JavaScript - Single Page Application (SPA)
====================

An overview of the concept of SPA application and related techniques to build SPA and non-spa applications using HTML, CSS and JavaScript.

Showing some Web API, with code samples, that can be used when developing SPA applications. 

Lecture covering topics like:

* SPA as an application architecture
* Pros and cons
* Related techniques to build webapps
* HTML API, useful for SPA
    * Online/Offline
    * WebSockets
    * Server-sent events
    * Web workers

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L17-js-spa/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 42 minutes long.

[![2022-12-06](https://img.youtube.com/vi/q7l2a0fFyjM/0.jpg)](https://www.youtube.com/watch?v=q7l2a0fFyjM)

<!--
The code examples in the lecture can be viewed and tried out in the example "[Event example code](https://gitlab.com/mikael-roos/html-css-js/-/tree/main/public/example/js/events)".
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

About SPA.

* [Wikipedia: Single-page application](https://en.wikipedia.org/wiki/Single-page_application)
    * [Isomorphic JavaScript](https://en.wikipedia.org/wiki/Isomorphic_JavaScript)
* [MDN Glossary: SPA (Single-page application)](https://developer.mozilla.org/en-US/docs/Glossary/SPA)
    * [MDN: Understanding client-side JavaScript frameworks](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks)

An overview of the many Web APIs.

* [Web APIs](https://developer.mozilla.org/en-US/docs/Web/API)

More types of application architecture related to SPA.

* [Electron](https://www.electronjs.org/)
* [Apache Cordova](https://cordova.apache.org/)
* [Progressive Web Apps](https://web.dev/progressive-web-apps/)
