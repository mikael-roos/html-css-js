---
author: mos
revision: 
    2022-12-06: "(A, mos) first version"
---
JavaScript - The Event Loop
====================

A walkthrough of the runtime environment in JavaScript with focus on the message queue and the event loop.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L15-js-event-loop/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 15 minutes long.

[![2022-12-06](https://img.youtube.com/vi/9EJBFBxljUo/0.jpg)](https://www.youtube.com/watch?v=9EJBFBxljUo)

<!--
The code examples in the lecture can be viewed and tried out in the example "[JavaScript general language programming constructs](https://gitlab.com/mikael-roos/html-css-js/-/tree/main/public/example/js/programming-constructs)".
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. [MDN on the Event Loop](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop)
