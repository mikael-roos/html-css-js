---
author: mos
revision: 
    2022-11-04: "(A, mos) first version"
---
CSS - Standards, Constructs and Structure
====================

We go through the basics of CSS and start from the standard and look at structure, constructions and how it works.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L03-css/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 40 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/yyostH3g7vk/0.jpg)](https://www.youtube.com/watch?v=yyostH3g7vk)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

W3C

* [CSS homepage](https://www.w3.org/Style/CSS/Overview.en.htm)
* [CSS current snapshot specification](https://www.w3.org/TR/CSS/)
* [CSS 2.1 specification](https://www.w3.org/TR/CSS2/) with the basics of CSS.
* [CSS validator](http://www.css-validator.org/)
* [Unicorn validator](https://validator.w3.org/unicorn/)

MDN

* The basics of "[CSS: Cascading Style Sheets](https://developer.mozilla.org/en-US/docs/Web/CSS)".
* More on the basics of CSS, "[CSS building blocks](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks)".
* [CSS Box Model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model).

W3Schools

* [CSS Tutorial](https://www.w3schools.com/css/default.asp)

Wikipedia

* [CSS](https://en.wikipedia.org/wiki/CSS)
