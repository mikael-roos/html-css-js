---
author: mos
revision: 
    2022-11-04: "(A, mos) first version"
---
HTML - Standards, Elements and Structure
====================

We go through the basics of HTML and start from the standard and look at structure, elements and how it works.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L02-html/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

Standardisation.

* [WHATWG standardisationorgan](https://whatwg.org/)
* [HTML Living Standard](https://html.spec.whatwg.org/)
* [DOM Living Standard](https://dom.spec.whatwg.org/)
* [WHATWG more standards](https://spec.whatwg.org/)
* [Memorandum of Understanding Between W3C and WHATWG](https://www.w3.org/2019/04/WHATWG-W3C-MOU.html)

Get going and learn.

* [MDN HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
* [MDN HTML elements reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
* [W3Schools HTML](https://www.w3schools.com/html/)
* [What are browser developer tools?](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_are_browser_developer_tools)
* [W3C cheatsheet](https://www.w3.org/2009/cheatsheet/)
* [How browser rendering works — behind the scenes](https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/)
* [CanIUse](https://caniuse.com/)
* [CodePen](https://codepen.io/)

Validate your pages.

* [HTML validator](https://validator.w3.org/)
* [HTML validator (nu)](https://validator.w3.org/nu/)
* [Unicorn](https://validator.w3.org/unicorn/)
* [Link checker](https://validator.w3.org/checklink)

Related and useful.

* [MIME/Media types](https://en.wikipedia.org/wiki/Media_type)
* [Unicode](https://en.wikipedia.org/wiki/Unicode)
* [HTML entities](https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references)
* [MDN HTML entities reference sheet](https://dev.w3.org/html5/html-author/charref)
