---
author: mos
revision: 
    2022-11-04: "(A, mos) first version"
---
Responsiv webbdesign
====================

The lecture answers questions about responsive web design and what the components are in a responsive website and which techniques are behind this way of designing a website.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L06-responsive-web-design/slide.html).



Resurser
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

* web.dev: [Responsive web design basics](https://web.dev/responsive-web-design-basics/)
* MDN: [Responsive design](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Responsive_Design)
* W3Schools: [HTML Responsive Web Design](https://www.w3schools.com/html/html_responsive.asp)
* Wikipedia: [Responsive web design](https://en.wikipedia.org/wiki/Responsive_web_design)
* [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [How to create responsive typography using CSS — Three different methods explained](https://dev.to/laurilllll/how-to-create-responsive-typography-using-css-three-different-methods-explained-50f8)
* [Using CSS custom properties (variables)](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)
* [The Responsive Website Font Size Guidelines](https://learnui.design/blog/mobile-desktop-website-font-size-guidelines.html)

Related techniques.

* [Adaptive web design](https://en.wikipedia.org/wiki/Adaptive_web_design)
* [Unobtrusive JavaScript](https://en.wikipedia.org/wiki/Unobtrusive_JavaScript)
* [Progressive enhancement](https://en.wikipedia.org/wiki/Progressive_enhancement)
