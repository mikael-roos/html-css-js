---
author: mos
revision: 
    2022-11-22: "(A, mos) first version"
---
JavaScript - Programming
====================

We go through some basic language constructs in the JavaScript programming language. If you have programmed before then these are the constructs that help you build your first program in JavaScript.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L12-javascript-programming/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->

The code examples in the lecture can be viewed and tried out in the example "[JavaScript general language programming constructs](https://gitlab.com/mikael-roos/html-css-js/-/tree/main/public/example/js/programming-constructs)".



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. Learn from the free book "[JavaScript for impatient programmers (ES2022 edition)](https://exploringjs.com/impatient-js/index.html).

1. Use the [JavaScript reference on MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference).

    1. [Standard built-in objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)

    1. [JavaScript modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

    1. [JavaScript classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)

1. Learn through the [W3Schools: JavaScript Tutorial](https://www.w3schools.com/js/)
