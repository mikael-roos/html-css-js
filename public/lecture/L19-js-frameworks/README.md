---
author: mos
revision: 
    2022-12-06: "(A, mos) first version"
---
JavaScript - The Event Model, about Events
====================

A walkthrough of how events works in JavaScript, including

* Bubbling
* Capture
* Prevent default
* Event object
* Synthetic events

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L16-js-events/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 30 minutes long.

[![2022-12-06](https://img.youtube.com/vi/kv6tsmkR-og/0.jpg)](https://www.youtube.com/watch?v=kv6tsmkR-og)

The code examples in the lecture can be viewed and tried out in the example "[Event example code](https://gitlab.com/mikael-roos/html-css-js/-/tree/main/public/example/js/events)".



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

Popular SPA frameworks.

* [React](https://reactjs.org/)
* [Vue.JS](https://vuejs.org/)
* [Angular](https://angular.io/)
* [Mithril](https://mithril.js.org/)

