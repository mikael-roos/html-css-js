---
revision: 
    2022-11-08: "(A, mos) first version"
---
Web technologies
========================

We look at the concept of web technologies as an introduction to the techniques for building websites and web services.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L01-web-technologies/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

Recorded presentation, 44 minutes long (Swedish).

[![2022-11-08 swe](https://img.youtube.com/vi/oGSiiGlTAOk/0.jpg)](https://www.youtube.com/watch?v=oGSiiGlTAOk)

<!--
Record small exercise work with accessing web server using terminal and devtools.
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

For the fun of it.

* [First website ever, info.cern.ch](http://info.cern.ch/hypertext/WWW/TheProject.html).
* [First web browser](https://worldwideweb.cern.ch/)
* [Wayback machine, check out old websites](https://archive.org/web/).

Web technologies through Wikipedia.

* [URL](https://en.wikipedia.org/wiki/URL)
* [HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [HTTPS](https://en.wikipedia.org/wiki/HTTPS)
* [HTML](https://en.wikipedia.org/wiki/HTML), [CSS](https://en.wikipedia.org/wiki/CSS), [JavaScript](https://en.wikipedia.org/wiki/JavaScript)
* [DOM](https://en.wikipedia.org/wiki/Document_Object_Model)
* [Web browser](https://en.wikipedia.org/wiki/Web_browser)
* [Web server](https://en.wikipedia.org/wiki/Web_server)

Learn and practice HTML and CSS.

* [MDN HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
* [MDN CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
* [W3Schools HTML](https://www.w3schools.com/html/)
* [W3Schools CSS](https://www.w3schools.com/css/)
