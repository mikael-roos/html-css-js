---
author: mos
revision: 
    2022-11-22: "(A, mos) first version"
---
JavaScript - Introduction
====================

In this introduction to the programming language JavaScript we get introduced to aspects as:

* About, History, Standard
* Client side versus Server side
* Eco system and Frameworks

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/html-css-js/lecture/L11-javascript-intro/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

Wikipedia

* [JavaScript](https://en.wikipedia.org/wiki/JavaScript)
* [ECMAScript](https://en.wikipedia.org/wiki/ECMAScript)
* [JavaScript engine](https://en.wikipedia.org/wiki/JavaScript_engine)
    * [V8 (JavaScript engine)](https://en.wikipedia.org/wiki/V8_(JavaScript_engine))
* [Node.js](https://en.wikipedia.org/wiki/Node.js)

Home of techniques

* [MDN JavaScript reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
* [Node.js](https://nodejs.org/)
    * [Node.js releases](https://github.com/nodejs/release#release-schedule)
    * [Node.js API documentation](https://nodejs.org/en/docs/)
* [NPM](https://www.npmjs.com/)

Books on JavaScript

* [Exploring JS: JavaScript books for programmers](https://exploringjs.com/)

Guide & Tutorials

* [MDN JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
* [W3Schools JavaScript Tutorial](https://www.w3schools.com/js/)

Video

* [Crockford on JavaScript – Volume 1: The Early Years](https://youtu.be/JxAXlJEmNMg) (1 h 42 m). Douglas Crockford give a historical background to JavaScript.

