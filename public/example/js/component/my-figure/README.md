---
revision:
    "2024-11-30": "(A, mos) First release."
---
Web component: My Figure element
===========================

Describe how to work with a web component.


[[_TOC_]]



Prepare
---------------------------

You know the basics of JavaScript and how it works with the browser environment.



Directory structure
---------------------------

The directory structure for this exercise, when it is done, looks like this.

```text
$ tree .                 
.                        
├── README.md            
├── css                  
│   └── style.css        
├── img                  
│   └── glider.png       
├── js                   
│   ├── main.js          
│   └── modules          
│       ├── Dice.js      
│       └── Player.js    
└── page.html            
                         
4 directories, 7 files   
```



Recording
---------------------------

This is the live coding session where Mikael creates the code now stored in this repo (67 min).

[![2022-11-29](https://img.youtube.com/vi/zy1fM8Ey-IU/0.jpg)](https://www.youtube.com/watch?v=zy1fM8Ey-IU)
