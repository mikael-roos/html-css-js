/**
 * The HTML for the component is saved in its own file.
 */
export const htmlTemplate = document.createElement('template')

htmlTemplate.innerHTML = `
  <figure>
    <img src="" alt="">
    <figcaption></figcaption>
  </figure>
`

