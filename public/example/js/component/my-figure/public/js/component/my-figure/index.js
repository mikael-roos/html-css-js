/**
 * A web component for a figure element.
 */
import { cssTemplate } from './template.css.js'
import { htmlTemplate } from './template.html.js'

customElements.define('my-figure',
  class extends HTMLElement {

    #img
    #caption

    /**
     * Initialize and attach shadow DOM and append templates.
     */
    constructor () {
      super()

      // Attach shadow DOM and allow it to be modified from js
      this.attachShadow({ mode: 'open' })

      // Append the styles and templates to the shadow DOM
      this.#appendTemplate()
      
      // Save references to parts of the element
      this.#img = this.shadowRoot.querySelector('img');
      this.#caption = this.shadowRoot.querySelector('figcaption');

    }

    /**
     * Append CSS and HTML templates to shadow DOM.
     */
    #appendTemplate () {
      // Helper method to append CSS and HTML
      this.shadowRoot.appendChild(cssTemplate.content.cloneNode(true))
      this.shadowRoot.appendChild(htmlTemplate.content.cloneNode(true))
    }

    /**
     * Lazy initialize shadow DOM references.
     */
    #initializeShadowDOMReferences () {
      if (!this.#img || !this.#caption) {
        this.#img = this.shadowRoot.querySelector('img')
        this.#caption = this.shadowRoot.querySelector('figcaption')
      }
    }

    /**
     * Render the component.
     */
    render () {
      // Lazy initialization of shadow DOM references
      this.#initializeShadowDOMReferences()
      
      // Update each attribute if it exists
      this.#img.src = this.hasAttribute('src')
        ? this.getAttribute('src')
        : ""
      
      this.#img.alt = this.hasAttribute('alt')
        ? this.getAttribute('alt')
        : "Missing alt text"

      this.#caption.textContent = this.hasAttribute('caption') 
        ? this.getAttribute('caption')
        : "Caption is missing"
    }

    /**
     * Attributes to monitor for changes.
     *
     * @returns {string[]} A string array of attributes to monitor.
     */
    static get observedAttributes () {
      return ['src', 'alt', 'caption']
    }

    /**
     * Called when observed attribute(s) changes.
     *
     * @param {string} name of the attribute.
     * @param {any} oldValue the old attribute value.
     * @param {any} newValue the new attribute value.
     */
    attributeChangedCallback (name, oldValue, newValue) {
      // Only update the specific element related to the changed attribute
      if (name === 'src' && oldValue !== newValue) {
        this.#img.src = newValue
      } else if (name === 'alt' && oldValue !== newValue) {
        this.#img.alt = newValue
      } else if (name === 'caption' && oldValue !== newValue) {
        this.#caption.textContent = newValue
      }
    }

    /**
     * Called after the element is inserted into the DOM.
     */
    connectedCallback () {
      this.render()
    }

    /**
     * Called after the element has been removed from the DOM.
     */
    disconnectedCallback () {
      // Clean up any resources or event listeners if needed
    }
  }
)
