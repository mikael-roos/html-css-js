/**
 * The CSS for the component is saved in its own file.
 */
export const cssTemplate = document.createElement('template')

cssTemplate.innerHTML = `
<style>
:host {
  display: flex;
  justify-content: left; 
}

:host(.center) {
  clear: both;
  justify-content: center;
  text-align: center;
}

:host(.left) {
  float: left;
  margin-right: 1em;
}

:host(.right) {
  float: right;
  margin-left: 1em;
}

:host(.width100) figure{
  width: 100%;
}

:host(.width50) figure {
  width: 50%;
}

:host(.width100) img,
:host(.width50) img {
  width: 100%;
}

:host::after {
  content: "";
  display: table;
  clear: both;
}

figure {
  display: table;
  padding: 0;
  margin: 0;
  margin-bottom: 24px;
}

figure img {
  max-width: 100%; /* Make images responsive */
  display: block;
}

figure figcaption {
  display: table-caption;
  caption-side: bottom;
  margin-top: 0;
  margin-bottom: 0;
  font-style: italic;
  padding-top: 0.5em;
}

</style>
`
