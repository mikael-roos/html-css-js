/**
 * Module exporting cariables and functions.
 */
let sum = 0

/**
 * Increment a value.
 */
function increment () {
  sum += 1
}

export { sum, increment }
