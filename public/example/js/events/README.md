---
revision:
    "2022-12-06": "(A, mos) First release."
---
Event example code
===========================

This example is built to exemplify events and the sample code is used in a slide lecture.

[[_TOC_]]

<!--
This is how it can look like when we are done with this exercise. Still, it is the code behind it that is important.

![Done](.img/done.png)
-->

