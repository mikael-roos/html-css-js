---
revision:
    "2023-11-21": "(B, mos) Worked through it."
    "2022-11-18": "(A, mos) First release."
---
Roll dices in a webpage using JavaScript
===========================

This example program shows how to roll a set of dices when the user clicks a button.

[[_TOC_]]

This is how it can look like when we are done with this exercise. Still, it is the code behind it that is important.

![Done](.img/done.png)



Live example
---------------------------

You can try out [the live example here](https://mikael-roos.gitlab.io/html-css-js/example/js/roll-dices/page.html).

View its source code it you are in need of the solution for this exercise.



Prepare
---------------------------

You should have walked through the exercise "[A 'Hello World' example with JavaScript in a webpage](../hello-world/)".



Directory structure
---------------------------

Do create a directory, for example `public/roll-dices` where you can work in.

The directory structure for this exercise, when it is done, might look like this.

```text
$ tree .
.
├── css
│   └── style.css 
├── js
│   └── main.js
└── page.html
```



Write the code yourself
---------------------------

Can you try to write the code youreself? What is needed?

1. Start with a web page `page.html` that includes a stylesheet `css/style.css` and the javascript file `js/main.js`.

1. To the web page, add a title and a button.

1. Add an eventhandler to the button when it is clicked so a function is called.

    * Ensure that the function is called when clicking the button by printing out some information in the console.

1. In the called function, roll some dice using random numbers and add the results to the DOM.

1. There are UTF-8 characters that represents the dice which you can use.
    * ⚀
    * ⚁
    * ⚂
    * ⚃
    * ⚄
    * ⚅



Practice more
---------------------------

If you succeed with this exercise, then proceed to the exercise where you should [write your code using functions in modules](../roll-dices-module).
