---
revision:
    "2022-11-18": "(A, mos) First release."
---
JavaScript general language programming constructs
===========================

This example shows some basic language constructs in JavaScript, the code was created to support examples used in a lecture.

Open the page in a browser and open devtools console to view the output.

Study the source code and see its output in the console. 
