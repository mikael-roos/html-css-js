Example repo for HTML, CSS, JavaScript
=========================

This is an example repo for HTML, CSS and JavaScript. It contains code samples, exercises, guides and lecture material for working with programming and development using HTML, CSS and JavaScript.

You may call it "the teachers repo".

The repo also contains a development environemnt with tools and linters. Check it out by executing `npm run`.

After cloning the repo, start by installing the tools and linters.

```
npm install
```

You can then start a web server to access the web pages in the `public/` directory.

```
npm run http-server
```
