/**
 * Function to return a message.
 * @returns {string} With the hello message.
 */
function hello () {
  return 'Hello world'
}

console.log(hello())
